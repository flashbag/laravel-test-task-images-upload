<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('index');

Route::get('/upload', 'HomeController@uploadGet')->name('upload.get');
Route::post('/upload', 'HomeController@uploadPost')->name('upload.post');

Route::group(['middleware' => 'auth.basic.once'], function () {
    Route::resource('/files', 'UploadedFilesController')->only(['index','destroy']);
});



