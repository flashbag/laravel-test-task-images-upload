<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\UploadedImage;

class UploadedFilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $items = UploadedImage::paginate(20);

        return view('files.list', [
            'items' => $items
        ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  UploadedImage $file
     * @return \Illuminate\Http\Response
     */
    public function destroy(UploadedImage $file)
    {
        Storage::disk('local')->delete($file->path);

        $diskPublic = Storage::disk('public');

        $diskPublic->delete($file->path_m);
        $diskPublic->delete($file->path_s);

        $file->delete();
    }
}
