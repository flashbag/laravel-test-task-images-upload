<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\{
    DB, Storage
};


use App\Events\ImageUploadedEvent;

use App\Models\UploadedImage;
use App\Http\Requests\UploadImagesRequest;
/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{

    public function index()
    {
        return view('index');
    }

    public function uploadGet()
    {
        return view('upload');
    }

    public function uploadPost(UploadImagesRequest $request)
    {
        $data = $request->validated();

        $filesCount = count($data['files']);

        DB::beginTransaction();

        foreach($data['files'] as $file) {

            $path = Storage::putFile('images', $file);

            $uploadedImage = UploadedImage::create([
                'mimeType' => $file->getClientMimeType(),
                'originalName' => $file->getClientOriginalName(),
                'size' => Storage::size($path),
                'path' => $path
            ]);

            event(new ImageUploadedEvent($uploadedImage));
        }

        DB::commit();

        return redirect()
            ->back()
            ->with('success', "Successfully uploaded ${filesCount} files!");
    }
}
