<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UploadedImage extends Model
{
    protected $fillable = [
        'mimeType',
        'originalName',
        'size',

        'path',

        'path_s',
        'path_m',
    ];

    public function getNameAttribute()
    {
        $parts = explode('/', $this->path);

        return $parts[count($parts) -  1];
    }
}
