<?php

namespace App\Listeners;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Contracts\Queue\ShouldQueue;

class ImageUploadedListener implements ShouldQueue
{
    protected $tries = 3;

    protected $mediumFormat = 'jpg';
    protected $thumbFormat = 'jpg';

    protected $mediumQuality = 90;
    protected $thumbQuality = 90;

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $pathMedium = $this->resizeImage($event->file,
            config('image.size_medium_width'),
            config('image.medium_dir')
        );

        $pathThumb = $this->resizeImage($event->file,
            config('image.size_thumb_width'),
            config('image.thumb_dir')
        );

        $event->file->update([
            'path_m' => $pathMedium,
            'path_s' => $pathThumb,
        ]);

    }

    private function resizeImage($uploadedFile, $width, $directory)
    {
        $tmpImageName = tempnam("/tmp", "resized_image");

        $fullPath = storage_path('app/' . $uploadedFile->path);
        $image = Image::make($fullPath);

        // resize the image to a width of 300 and constrain aspect ratio (auto height)
        $image->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $image->save($tmpImageName, $this->mediumQuality, $this->mediumFormat);

        $fileObject = new File($fullPath);
        $originalName = str_replace('.'. $fileObject->getExtension(), '', $fileObject->getBasename());

        var_dump('width::', $width );
        var_dump('directory::', $directory );

        $path = Storage::disk('public')->putFileAs(
            $directory,
            $tmpImageName,
            ($originalName . '.' . $this->mediumFormat)
        );

        unlink($tmpImageName);
        return $path;

    }

}
