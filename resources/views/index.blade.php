@extends('layouts.app')

@section('content')

    <div class="jumbotron">
        <h1 class="display-4">Laravel Images Uploader Test Task</h1>
        <p class="lead" style="display: none;">
            This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.
        </p>
        <hr class="my-4">
        <p>
            <ul>
                <li> - Аплоадить можно как по одной картинке, так и пачкой до 10 файлов.</li>

                <li> - Картинки должны ресайзится для 2-ух размеров: большая картинка и миниатюра.</li>


                <li>- Оригинал должен хранится отдельно и никаких ресайзов, кропов
                с ним проводить не надо (кроме изменения названия).- Оригинал должен хранится отдельно и никаких ресайзов, кропов
                с ним проводить не надо (кроме изменения названия).</li>

                <li>- Оригинал хранится в приватном сторейдже, большая картинка и миниатюра в публичном.</li>
                <li>- Для миниатюр создается папка thumbs.</li>

                <li>- Названия оригинала, большой и маленькой картинки совпадают.</li>
                <li>- Поддиректория для картинок должна создаваться автоматически, если таковой нет.</li>
                <li>- Название картинки должно генерироваться</li>

            </ul>

        </p>

        <p class="lead">
            <a class="btn btn-primary btn-lg" href="/index.php/upload" role="button">Learn more</a>
        </p>
    </div>


@endsection
