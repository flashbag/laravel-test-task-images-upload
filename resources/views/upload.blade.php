@extends('layouts.app')

@section('content')

    <form action="{{ route('upload.post') }}" method="post" enctype="multipart/form-data">

        @csrf

        <div class="row justify-content-center">

            <div class="col-md-6">

                <h2>Upload Images</h2>

                <br>

                <p>Upload at least one image file below.</p>
                <p>You can add more files by clicking "Add file" button</p>

            </div>

            <div class="col-md-3 text-right">

                <a class="btn btn-primary btn-lg"  id="add-file" role="button">Add file</a>

            </div>
        </div>


        <div class="row justify-content-center">

            <div class="col-md-9" id="file-inputs">
                <input type="file" name="files[0]">
            </div>

        </div>


        <div class="row justify-content-center">
            <div class="form-group col-md-9">
                <br>
                <hr>
                <br>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="form-group col-md-9">
                <button type="submit" class="btn btn-block btn-success">Submit</button>
            </div>
        </div>

    </form>

@endsection


@push('scripts')

    <script type="text/javascript">

        $(document).ready(function(){

            let $btnAddFile = $('#add-file');
            let $fileInputs = $('input[type="file"]');
            let $fileInputsContainer = $('#file-inputs');

            $btnAddFile.on('click', function(){
                $fileInputs = $('input[type="file"]');

                let newInputIndex = $fileInputs.length;

                $fileInputsContainer.append('<br><br>');
                $fileInputsContainer.append('<input type="file" name="files[' + newInputIndex + ']">');

            });

        });

    </script>
@endpush
