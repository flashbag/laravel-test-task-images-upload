@extends('layouts.app')

@section('content')

  <div class="row">

  <div class="col-md-6 offset-md-3">
        <h2>edit user {{ $item->email }}</h2>
    </div>

  </div>

<br/>

@if ($errors)
  @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
  @endforeach
@endif


<form method="POST" action="{{ $action }}">

  {{ csrf_field() }}
  {!! method_field($method) !!}

  <input type="hidden" name="id" value="{{ $item['id'] or null }}">

  <div class="row">
    <div class="col-md-3 offset-md-3">
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text">name</span>
        </div>
        <input type="text" class="form-control" value="{{ $item->name }}" name="name">
      </div>
    </div>
    <div class="col-md-3">
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text">role</span>
        </div>
        <input type="text" class="form-control" readonly="readonly" value="{{ $item->roles()->first()->name }}">        
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-3 offset-md-3">
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text">telegram id</span>
        </div>
        <input type="text" class="form-control" readonly="readonly" value="{{ $item->telegram_id }}" name="email">


      </div>
    </div>
    <div class="col-md-3">
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text">email</span>
        </div>
        <input type="text" class="form-control" readonly="readonly" value="{{ $item->email }}">
      </div>
    </div>
  </div>

  <div class="row">
    <div class="form-group col-md-6 offset-md-3">
      <label class="control-label" for="sub_id_name">
          select sub id: <span class="text-danger"> </span>
      </label>
      <br>
      <select name="sub_ids_ids[]" id="sub_ids_ids" multiple>
        @foreach($item->subIds as $subId)
          <option selected="selected" value="{{ $subId->id }}">{{ $subId->name }}</option>
        @endforeach
      </select>
    </div>
  </div>

  <div class="row">
    <div class="form-group col-md-6 offset-md-3">
      <button type="submit" class="btn btn-block btn-success">update</button>
    </div>
  </div>


</form>


@endsection


@push('style')
 
@endpush

@push('scripts')
  
  <script type="text/javascript">

    function subIdsSelect2($select, route) {

      let sub_ids_ids = [];

      $select.find('option').each(function(){
        sub_ids_ids.push($(this).val());
      });

      $select.on('select2:select', function (e) {
        var data = e.params.data;
        sub_ids_ids.push(data.id);
      });

      // console.log('existing users ids:', sub_ids_ids);

      $select.select2({
        placeholder: "select sub id",
        minimumInputLength: 2,
        ajax: {
          url: route,
          dataType: 'json',
          data: function (params) {
            return {
              query: params.term,
              page: params.page,
              sub_ids_ids: sub_ids_ids,
              _token: '{{ csrf_token() }}'
            };
          },
          processResults: function (data, params) {
            return {
              results: data.results,
              pagination: {
                more: data.more
              }
            };
          },
          cache: false
        }
      });
    }

    $(document).ready(function() {

        console.log('document ready');

        subIdsSelect2($('#sub_ids_ids'), '{{ route('admin.postback.sub-ids.search') }}');
    });
  </script>
@endpush
