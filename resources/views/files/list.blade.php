@extends('layouts.app')

@section('content')

<div class="row">

	<div class="col-md-6">
		<h2>Files</h2>
	</div>
</div>

<br>

<table class="table table-bordered">
	<thead>
		<tr class="text-center">

			<th scope="col">id</th>
            <th scope="col">date</th>

			<th scope="col">original name</th>
			<th scope="col">name</th>
			<th scope="col">size</th>
            <th scope="col">links</th>


			<th scope="col">actions</th>

		</tr>
	</thead>
	<tbody>

		@foreach($items as $file)

		<tr class="text-center">

			<td class="align-middle">
				{{ $file->id }}
			</td>

            <td class="align-middle">
                {{ $file->created_at }}
            </td>

			<td class="align-middle">
				{{ $file->originalName }}
			</td>
			<td class="align-middle">
				{{ $file->name }}
			</td>
            <td class="align-middle">
                {{ $file->size }}
            </td>

            <td class="align-middle">
                @if($file->path_s && $file->path_m)
                    <a target="_blank" href="/storage/{{ $file->path_m }}">medium</a>
                    <br>
                    <a target="_blank" href="/storage/{{ $file->path_s }}">thumb</a>
                @endif
            </td>

			<td class="align-middle" >
                <a style="" data-href="{{ route('files.destroy', $file->id) }}" class="remove-status" data-item-id="{{ $file->id }}">
                    <i class="fa fa-close icon-close text-danger"></i>
                    Remove
                </a>
			</td>
		</tr>

		@endforeach
	</tbody>
</table>


<div class="text-center">
{{--	{!! $items->appends(\Request::except('page'))->render() !!}--}}
</div>

@endsection

@push('scripts')
    <script>

        $(function(){


            function removeItem(url) {

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {
                        "_token": '{{ csrf_token() }}',
                        "_method": 'DELETE'
                    },
                    success: function (response) {
                        location.reload();
                    },
                    error: function (response) {
                        alert('Ошибка!')
                    }
                });

            }

            $('.remove-status').on('click', function (e) {

                e.preventDefault();

                var self = this;

                var isConfirmed = confirm('Do you confirm removing file?');

                if (!isConfirmed) {
                    return false;
                }

                // alert(isConfirmed);
                removeItem($(self).data('href'));

            });

        });

    </script>
@endpush
