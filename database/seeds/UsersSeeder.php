<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getUsers() as $user) {

            $user = User::firstOrCreate([
                'email' => $user['email']
            ], $user);

        }
    }

    private function getUsers()
    {
        return [
            [
                'name' => 'Admin',
                'email' => 'admin@site.com',
                'password' => Hash::make('qwerty'),
            ]
        ];
    }

}
