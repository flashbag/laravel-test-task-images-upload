<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',

    'size_medium_width' => env('IMAGE_RESIZE_MEDIUM_WIDTH', 600),
    'size_thumb_width' => env('IMAGE_RESIZE_THUMB_WIDTH', 200),

    'medium_dir' => env('IMAGE_RESIZE_MEDIUM_DIR', 'images'),
    'thumb_dir' => env('IMAGE_RESIZE_THUMB_DIR', 'thumbs'),

];
